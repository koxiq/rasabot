# -*- coding: utf-8 -*-
from typing import Dict, Text, Any, List, Union, Optional

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction 


from rasa.core.events import SlotSet 
from rasa_sdk import Action

import urllib.request
import urllib.parse  
 

class RecordsForm(FormAction):
    """Example of a custom form action"""

    def name(self) -> Text:
        """Unique identifier of the form"""

        return "records_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""
        return ["date"]

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""
 
        return {
            "date": self.from_entity(entity="date"),
        }

    @staticmethod
    def is_int(string: Text) -> bool:
        """Check if a string is an integer"""

        try:
            int(string)
            return True
        except ValueError:
            return False

    # USED FOR DOCS: do not rename without updating in docs
    def validate_date(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Optional[Text]:
        """Validate cuisine value."""

        if str(value):
            # validation succeeded, set the value of the "cuisine" slot to value
            return {"date": value}
        else:
            dispatcher.utter_template("utter_wrond_date", tracker)
            # validation failed, set this slot to None, meaning the
            # user will be asked for the slot again
            return {"date": None}

    

    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""

        data = {} 
        data['year'] = 2012  
        url_values = urllib.parse.urlencode(data) 

        url = 'https://calendarin.net/calendar.php' 
         
        full_url = url + '?' + url_values 
          
        # utter submit template 
        dispatcher.utter_template("utter_to_record", tracker, reference=full_url)
        
        
        return []  

class DietForm(FormAction): 
      
    @staticmethod
    def is_int(string: Text) -> bool:
        try:
            int(string)
            return True
        except ValueError:
            return False  

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "diet_form"
     
    @staticmethod 
    def body_size_test(n: int) -> bool:
        return n>=20 and n<=150
    
    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        return  ["age", "training_level",
         "hips", "waist", "chest" , "height" ,"weight", "preferences", "feed_count", "gender"] 

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {
            "gender": self.from_entity(entity="gender"), 
            "age": self.from_entity(entity="number"),  
            "training_level": self.from_entity(entity="number"),  
            "hips": self.from_entity(entity="number"),  
            "chest": self.from_entity(entity="number"),   
            "waist": self.from_entity(entity="number"),  
            "height": self.from_entity(entity="number"), 
            "weight": self.from_entity(entity="number"),  
            "preferences": self.from_entity(entity="preferences_entity"),  
            "feed_count": self.from_entity(entity="number"),  

        }
 
    @staticmethod
    def male_db():
        # type: () -> List[Text]
        return ["м","муж","мужской", "мужск"]    
         
    @staticmethod
    def female_db():
        # type: () -> List[Text]
        return ["ж","жен","женский", "женск"]   

    @staticmethod
    def gender_db():
        # type: () -> List[Text]
        return DietForm.male_db() + DietForm.female_db()
         

    @staticmethod
    def preferences_db():
        return ["сладкое", "острое", "соленое",  
                "кислое", "горькое", "жирное"] 
                 
	   
    def validate_gender(self,
                         value: Text,
                         dispatcher: CollectingDispatcher,
                         tracker: Tracker,
                         domain: Dict[Text, Any]) -> Optional[Text]:

        if value.lower() in self.gender_db():
            # validation succeeded
            return {"gender": value}
        else:
            dispatcher.utter_template('utter_wrong_gender', tracker)
            # validation failed, set this slot to None, meaning the
            # user will be asked for the slot again
            return {"gender": None}

    def validate_age(self,
                            value: Text,
                            dispatcher: CollectingDispatcher,
                            tracker: Tracker,
                            domain: Dict[Text, Any]) -> Optional[Text]:
 
        if value == None: 
            dispatcher.utter_template('utter_wrong_age', tracker) 
            return {"age": None} 
        if self.is_int(value) and int(value) >= 14 and int(value) <= 90:
            return {"age": value}
        else:
            dispatcher.utter_template('utter_wrong_age', tracker)
            # validation failed, set slot to None
            return {"age": None}   
         
    def validate_training_level(self,
                         value: Text,
                         dispatcher: CollectingDispatcher,
                         tracker: Tracker,
                         domain: Dict[Text, Any]) -> Optional[Text]:

        if self.is_int(value) and 0 <= int(value) <= 5:
            # validation succeeded
            return {"training_level": value}
        else:
            dispatcher.utter_template('utter_wrong_training_level', tracker)
            # validation failed, set this slot to None, meaning the
            # user will be asked for the slot again
            return {"training_level": None}  
             
    def validate_hips(self,
                            value: Text,
                            dispatcher: CollectingDispatcher,
                            tracker: Tracker,
                            domain: Dict[Text, Any]) -> Optional[Text]:

        if self.is_int(value) and self.body_size_test(int(value)):
            return {"hips": value}
        else:
            dispatcher.utter_template('utter_wrong_hips', tracker)
            # validation failed, set slot to None
            return {"waist": None}
         
    def validate_waist(self,
                            value: Text,
                            dispatcher: CollectingDispatcher,
                            tracker: Tracker,
                            domain: Dict[Text, Any]) -> Optional[Text]:

        if self.is_int(value) and self.body_size_test(int(value)):
            return {"waist": value}
        else:
            dispatcher.utter_template('utter_wrong_waist', tracker)
            # validation failed, set slot to None
            return {"waist": None}

    def validate_weight(self,
                            value: Text,
                            dispatcher: CollectingDispatcher,
                            tracker: Tracker,
                            domain: Dict[Text, Any]) -> Optional[Text]:

        if self.is_int(value) and self.body_size_test(int(value)):
            return {"weight": value}
        else:
            dispatcher.utter_template('utter_wrong_weight', tracker)
            # validation failed, set slot to None
            return {"weight": None}     
         
    def validate_height(self,
                            value: Text,
                            dispatcher: CollectingDispatcher,
                            tracker: Tracker,
                            domain: Dict[Text, Any]) -> Optional[Text]:

        if self.is_int(value) and int(value) > 100 and int(value) <= 250:
            return {"height": value}
        else:
            dispatcher.utter_template('utter_wrong_height', tracker)
            # validation failed, set slot to None
            return {"height": None}
     
    def validate_preferences(self,
                         value: Text,
                         dispatcher: CollectingDispatcher,
                         tracker: Tracker,
                         domain: Dict[Text, Any]) -> Optional[Text]:

        if value.lower() in self.preferences_db():
            # validation succeeded
            return {"preferences": value}
        else:
            dispatcher.utter_template('utter_wrong_preferences', tracker)
            # validation failed, set this slot to None, meaning the
            # user will be asked for the slot again
            return {"preferences": None} 
 
    def validate_feed_count(self,
                            value: Text,
                            dispatcher: CollectingDispatcher,
                            tracker: Tracker,
                            domain: Dict[Text, Any]) -> Optional[Text]:

        if self.is_int(value) and int(value)>0 and int(value) <=10:
            return {"feed_count": value}
        else:
            dispatcher.utter_template('utter_wrong_feed_count', tracker)
            return {"feed_count": None} 
          

    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:
     

        dispatcher.utter_template('utter_submit', tracker) 
        
        is_male = DietForm.male_db().count(tracker.get_slot("gender")) > 0
        
        age = int(tracker.get_slot("age"))
        training_level = int(tracker.get_slot("training_level")) 
        hips = int(tracker.get_slot("hips"))
        waist = int(tracker.get_slot("waist"))
        chest = int(tracker.get_slot("chest"))
        weight = int(tracker.get_slot("weight"))  
        height = int(tracker.get_slot("height"))
        ##parsing preferences   
        prefs = tracker.get_slot("preferences") 

        pref_value = 0
        if(prefs == "острое" or prefs == "кислое"): 
            pref_value = 1 

        if(prefs == "горькое" or prefs == "соленое"):
            pref_value = 2  
            
        if(prefs == "сладкое" or prefs == "жирное"): 
            pref_value = 3
             
        #parsing trainLevel 
        train_coef = 1.2 + (0.175 * training_level)
        
        feed_count = int(tracker.get_slot("feed_count")) 
 
        addend = 0
        if(is_male): 
            addend = 5 
        else: 
            addend = -161  
 
        res = 0
        res = (10 * weight + 6.25 * height - 5 * age + addend - 100 * pref_value + 50 * feed_count + (hips + waist + chest)/4 ) * train_coef
  


        dispatcher.utter_template("utter_result", tracker, result=int(res))  
         
        """
        SlotSet("age", None) 
        SlotSet("training_level", None)   
        SlotSet("hips", None) 
        SlotSet("waist", None)   
        SlotSet("chest", None) 
        """
          
        return []  
         
          
class ActionStoreIntentMessage(Action):

    def name(self):
        return "action_clear_diet_slots"

    def run(self, dispatcher, tracker, domain):

        # we grab the whole user utterance here as there are no real entities
        # in the use case
        #message = tracker.latest_message.get('text')

        return [
            SlotSet('age', None), 
            SlotSet('gender', None), 
            SlotSet('hips', None), 
            SlotSet('waist', None), 
            SlotSet('chest', None), 
            SlotSet('training_level', None), 
            SlotSet('feed_count', None), 
            SlotSet('height', None), 
            SlotSet('preferences', None),  
            SlotSet('weight', None), 
            ]