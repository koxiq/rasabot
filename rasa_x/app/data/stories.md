## Помощь
* help
  - help

## Запись в зал
* records
	- utter_start_records
    - records_form
    - form{"name": "records_form"}
	- form{"name": null}
* bad_say
    - utter_bad_response_2

## Привет снова

* hi
    - utter_say_hi
* hi
    - utter_hi_again 
* help 
	- action_deactivate_form

## Диета
* diet
    - utter_diet_start
    - diet_form 
    - form{"name": "diet_form"}
	- form{"name": null}
    - action_clear_diet_slots

## Диета2
* diet
    - utter_diet_start
    - diet_form 
    - form{"name": "diet_form"}
* bad_say 
    - utter_bad_response_2 
    - action_deactivate_form
    - form{"name": null}
    - action_clear_diet_slots

## Грубое
* bad_say
    - utter_bad_response

## Диета3
* give_info
* bad_say
    - utter_bad_response_2 
    - action_deactivate_form
    - form{"name": null}
    - action_clear_diet_slots
